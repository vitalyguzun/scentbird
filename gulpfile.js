'use strict';

const gulp = require('gulp');

gulp.task('deploy', () => {
    gulp.src(['./CNAME']).pipe(gulp.dest(`./build/`));
});
