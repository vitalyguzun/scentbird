import { forEach, keys, length, replace, compose, sum } from 'ramda';
import ERRORS from '../constants/errors';
import { notification } from 'antd';

const min = (min, exclude = '') => {
    const regexp = new RegExp(exclude, 'g');

    return (value) => {
        return length(replace(regexp, '', value)) >= min ? undefined : ERRORS.min(min);
    }
}

const required = (error) => {
    return (value) => {
        return !value ? error : undefined;
    }
}

const clearStr = compose(replace(/\s|_/g, ''));
let secureBuffer = '';

const validates = {
    required: required(ERRORS.required),
    requiredGender: required(ERRORS.requiredGender),
    min10: min(10),
    min3: min(3, '_'),
    notify111: (value, model) => {
        if (value === '111' && value !== secureBuffer) {
            secureBuffer = value;
            notification.open({ description: 'Wrong CVV2 code' });
        }

        return value === '111' ? ERRORS.wrongSecureCode : undefined;
    },
    creditCard: (value) => {
        const str = clearStr(value);

        if (length(str) < 16) {
            return ERRORS.required;
        }

        const evens = [];
        let odds = [];

        [...str].forEach((val, index) => {
            if (index % 2 === 0) {
                odds.push(val);
            } else {
                evens.push(val)
            }
        });

        odds = odds.map((el) => {
            let result = (el * 2).toString();

            if (length(result) > 1) {
                result = sum([...result]);
            }

            return +result;
        });

        return sum([...evens, ...odds]) % 10 === 0 ? undefined : ERRORS.wrongCardNumber;
    },
    email: (value) => {
        return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? ERRORS.wrongEmail : undefined;
    }
}

export default (objectFields) => {
    return (value) => {
        const errors = {};
        const arrayFields = keys(objectFields);

        forEach((field) => {
            forEach((validate) => {
                errors[field] = errors[field] || validates[validate](value[field]);
            }, objectFields[field]);
        }, arrayFields);

        return errors;
    }
}
