import { GET_DICTIONARY } from '../constants/actionTypes';

export default function disctionaryReducer(state = {}, action) {
    switch(action.type) {
        case GET_DICTIONARY:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
