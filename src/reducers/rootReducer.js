import { reducer as form } from 'redux-form';

import dictionaryReducer from './dictionaryReducer';
import productsReducer from './productsReducer';

export default {
    dict: dictionaryReducer,
    products: productsReducer,
    form
};
