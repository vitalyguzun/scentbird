import { GET_PRODUCT } from '../constants/actionTypes';

export default function disctionaryReducer(state = [], action) {
    switch(action.type) {
        case GET_PRODUCT:
            return [ ...state, action.payload ];
        default:
            return state;
    }
}
