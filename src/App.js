import React from 'react';
import { Provider } from 'react-redux';

import Logo from './components/logo/Logo';
import Product from './components/product/Product';
import Delivery from './components/common/delivery/Delivery';
import SubscribtionForm from './components/subscribtionForm/SubscribtionForm';

const App = ({ store }) => {
    const product = 0;

    return (
        <Provider store={store}>
            <div className="app">
                <Logo />
                <div className='content'>
                    <div className='product-container'>
                        <Product id={product} />
                        <Delivery />
                    </div>
                    <div className='form-container'>
                        <SubscribtionForm id={product} />
                    </div>
                </div>
            </div>
        </Provider>
    );
}

export default App;
