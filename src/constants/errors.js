export default {
    required: 'This field is required',
    requiredGender: 'Your gender is required',
    wrongEmail: 'Wrong email',
    min: (min) => `Minimum ${min} symbols`,
    wrongCardNumber: 'Wrong card number',
    wrongSecureCode: 'Wrong code'
}
