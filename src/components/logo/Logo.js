import React from 'react';

import logo from './assets/logo.png';

export default () => (
    <div className='logo-container'>
        <img src={logo} alt='scentbird-logo' />
    </div>
);
