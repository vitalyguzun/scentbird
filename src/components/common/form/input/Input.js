import React from 'react';
import cx from 'classnames';
import InputMask from 'react-input-mask';

import { QUESTION, VALID_FIELD } from '../../../../constants/addonTypes';
import Question from '../question/Question';
import ValidField from '../validField/ValidField';

const addons = {
    [QUESTION]: Question,
    [VALID_FIELD]: ValidField
}

const InputComponent = (props) => {
    const {
        className,
        meta: { invalid, submitFailed, error, active, touched, valid, dirty },
        meta,
        input,
        placeholder,
        addon,
        mask,
        ...rest
    } = props;

    const inputClasses = cx(`${className} input`, {
        invalid: invalid && (touched || submitFailed),
        valid: valid && touched && dirty
    });

    const containerClasses = cx({
        'input-container': true,
        focused: active || input.value,
        'text-center': mask,
        valid: valid && touched
    });

    const Addon = addons[addon];
    const ValidField = addons[VALID_FIELD];

    return (
        <div className={containerClasses}>
            <InputMask mask={mask} {...input} {...rest} className={inputClasses}/>
            { addon && <Addon /> }
            <ValidField />
            { placeholder && <label className='placeholder'>{ placeholder }</label> }
            { (touched || submitFailed) && error && <div className='error'>{ error }</div> }
        </div>
    );
}

export default InputComponent;
