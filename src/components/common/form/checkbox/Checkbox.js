import React from 'react';
import { Checkbox } from 'antd';

const CheckboxComponent = (props) => {
    const { label, ...rest } = props;

    const onChange = ({ target: { checked } }) => {
        props.input.onChange(checked);
    }

    return <Checkbox {...rest} onChange={onChange} checked={props.input.value}>{ label }</Checkbox>;
}

export default CheckboxComponent;
