import React from 'react';
import ReactSVG from 'react-svg';
import { Select } from 'antd';
import cx from 'classnames';
import { map, type } from 'ramda';

import carret from './assets/carret.svg';

const Option = Select.Option;

const SelectComponent = (props) => {
    const { items, meta: { invalid, submitFailed, error, touched, valid }, ...rest } = props;

    const renderItems = () => {
        return map((item) => {
            const name = type(item) === 'Object' ? item.name : item;

            return <Option key={name} value={name.toString()}>{name}</Option>;
        }, items);
    }

    const onChange = (value = '') => {
        const { onChange } = props.input;

        onChange(value);
    }

    const containerClasses = cx('input-container select-container', {
        invalid: invalid && (submitFailed || touched),
        valid
    });

    return (
        <div className={containerClasses}>
             <Select onChange={onChange} {...rest}>
                { renderItems() }
            </Select>
            <div className='carret'>
                <ReactSVG path={carret} />
            </div>
            { submitFailed && error && <div className='error'>{ error }</div> }
        </div>
    );
}

export default SelectComponent;
