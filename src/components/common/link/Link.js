import React from 'react';

const Link = ({ prefix, label, href, postfix }) => (
    <div className='link-sentence'>
        { prefix && <span>{ prefix }</span> }
        <a href={href} className='c-pink'>{ label }</a>
        { postfix && <span>{ postfix }</span> }
    </div>
);

export default Link;
