import React from 'react';

const InfoGroup = ({ label, content, currency }) => content ? (
    <div className='info-group'>
        { label && <div className='info-group-label'>{ label }</div> }
        { content &&
            <div className='info-group-content'>
                { currency && <span>$</span> }
                <span>{ content }</span>
            </div>
        }
    </div>
) : null;

export default InfoGroup;
