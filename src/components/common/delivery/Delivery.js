import React from 'react';
import { connect } from 'react-redux';

import delivery from './assets/delivery.png';

const Delivery = ({ subscribtionForm: dict, className = 'desk' }) => (
    <div className={`${className} delivery`}>
        <img src={delivery} alt='delivery' />
        { dict && <div className='delivery-text'>{ dict.confirmation }</div> }
    </div>
);

const stateToProps = ({ dict: {subscribtionForm} }) => ({ subscribtionForm });

export default connect(stateToProps)(Delivery);
