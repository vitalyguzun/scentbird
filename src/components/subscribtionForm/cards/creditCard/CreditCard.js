import React from 'react';
import { Field } from 'redux-form';

import { QUESTION } from '../../../../constants/addonTypes';
import lock from './assets/lock.png';
import cards from './assets/cards.png';
import Input from '../../../common/form/input/Input';
import Select from '../../../common/form/select/Select';
import { CREDIC_CARD_MASK, SECURE_CODE_MASK } from '../../../../constants/masks';

const monthes = [1,2,3,4,5,6,7,8,9,10,11,12];
const years = [17,18,19,20,21,22];

export default ({ subscribtionForm: dict }) => (
    <div className='card credit-card'>
        <h2 className='credit-card-title'>{ dict.secureCredit }</h2>
        <div className='credit-card-content'>
            <div className='flex info'>
                <div>
                    <img src={lock} alt='lock' className='lock' />
                </div>
                <div>{ dict.encryption }</div>
                <div>
                    <img src={cards} alt='cards' className='lock' />
                </div>
            </div>
            <div className='flex col-3-1 credit-numbers'>
                <Field name='creditCard'
                    component={Input}
                    placeholder={dict.placeholders.creditCard}
                    addon={QUESTION} mask={CREDIC_CARD_MASK} />
                <Field name='secureCode'
                    component={Input}
                    placeholder={dict.placeholders.secureCode}
                    addon={QUESTION} mask={SECURE_CODE_MASK} />
            </div>
            <div className='flex card-date'>
                <Field name='month'
                    component={Select}
                    items={monthes}
                    placeholder={dict.placeholders.month} />
                <Field name='year'
                    component={Select}
                    items={years}
                    placeholder={dict.placeholders.year} />
                <div className='exp'>Exp.</div>
            </div>
        </div>
    </div>
);
