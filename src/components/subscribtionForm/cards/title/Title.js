import React from 'react';

export default ({ title, subtitle, className }) => (
    <div className={`${className} card title`}>
        <h1>{ title }</h1>
        <div className='c-pink'>{ subtitle }</div>
    </div>
);
