import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, change } from 'redux-form';
import cx from 'classnames';
import { path } from 'ramda';

import Input from '../../../common/form/input/Input';

class Gender extends Component {

    setGender = (value) => {
        this.props.dispatch(change('subscribtionForm', 'gender', value));
    }

    render () {
        const { subscribtionForm: dict, form: { subscribtionForm: form}} = this.props;
        const gender = path(['values', 'gender'], form);
        const error = path(['syncErrors', 'gender'], form);
        const { submitFailed } = form;

        return (
            <div className='card gender'>
                <Field name='gender' component={Input} />
                <h2 className='gender-title'>{ dict.subscribtionType }</h2>
                <div className='gender-content'>
                    <div className='women-type' onClick={() => this.setGender('women')}>
                        <div className={cx('women-icon', { 'active': gender === 'women' })}>
                            <div className='check'></div>
                        </div>
                        <div>{ dict.forWomen }</div>
                    </div>
                    <div className='men-type' onClick={() => this.setGender('men')}>
                        <div className={cx('men-icon', { 'active': gender === 'men' })}>
                            <div className='check'></div>
                        </div>
                        <div>{ dict.forMen }</div>
                    </div>
                </div>
                { error && submitFailed && <div className='error'>{ error }</div> }
            </div>
        );
    }
};

const stateToProps = ({ form }) => ({ form });

export default connect(stateToProps)(Gender);
