import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field } from 'redux-form';
import { pick, path } from 'ramda';

import Address from './Address';
import Input from '../../../common/form/input/Input';
import Checkbox from '../../../common/form/checkbox/Checkbox';

class ShippingAddress extends Component {
    render () {
        const { subscribtionForm: dict, form: { subscribtionForm } } = this.props;
        const useThisAddressAsMyBilling = path(['values', 'billingAddress'], subscribtionForm);

        return (
            <div className='card shipping-address'>
                <h2 className='shipping-address-title'>{ dict.shippingAddress }</h2>
                <div className='shipping-address-content'>
                    <div className='flex col-1'>
                        <Field name='firstName'
                            component={Input}
                            placeholder={dict.placeholders.firstName} />
                        <Field name='lastName'
                            component={Input}
                            placeholder={dict.placeholders.lastName} />
                    </div>
                    <Address {...this.props} type='shipping' />
                    <div className='flex col-1 mobile-number'>
                        <Field name='mobileNumber'
                            component={Input}
                            placeholder={dict.placeholders.mobileNumber} />
                        <div className='label'>{ dict.weMaySendDiscount }</div>
                    </div>
                    <div className='flex'>
                        <Field name='billingAddress'
                            component={Checkbox}
                            label={dict.useThisAddressAsMyBilling} />
                    </div>
                    { !useThisAddressAsMyBilling &&
                        <div>
                            <h2 className='shipping-address-title'>{ dict.billingAddress }</h2>
                            <Address {...this.props} type='billing' />
                        </div>
                    }
                </div>
            </div>
        );
    }
};

export default connect(pick(['form']))(ShippingAddress);
