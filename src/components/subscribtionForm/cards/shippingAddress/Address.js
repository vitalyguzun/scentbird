import React from 'react';
import { Field } from 'redux-form';

import Input from '../../../common/form/input/Input';
import Select from '../../../common/form/select/Select';
import Errors from '../../../../constants/errors';

const required = (value) => {
    return !value ? Errors.required : undefined;
}

export default (props) => {
    const { subscribtionForm: dict } = props;

    return (
        <div className='address'>
            { props.type === 'shipping' &&
                <div>
                    <div className='flex col-2-1'>
                        <Field name='shippingStreet'
                            component={Input}
                            placeholder={dict.placeholders.streetAddress} />
                        <Field name='aptSuite'
                            component={Input}
                            placeholder={dict.placeholders.aptSuite} />
                    </div>
                    <div className='flex col-2-1'>
                        <div className='flex col-1'>
                            <Field name='shippingZipCode'
                                component={Input}
                                placeholder={dict.placeholders.zipCode} />
                            <Field name='shippingCity'
                                component={Select}
                                items={dict.cities}
                                placeholder={dict.placeholders.city} />
                        </div>
                        <Field name='shippingState'
                            component={Select}
                            items={dict.cities}
                            placeholder={dict.placeholders.state} />
                    </div>
                    <div className='flex col-1'>
                        <Field name='shippingCountry'
                            component={Input}
                            placeholder={dict.placeholders.country} />
                    </div>
                </div>
            }

            { props.type === 'billing' &&
                <div>
                    <div className='flex col-2-1'>
                        <Field name='billingStreet'
                            component={Input}
                            placeholder={dict.placeholders.streetAddress}
                            validate={[ required ]} />
                        <Field name='aptSuite'
                            component={Input}
                            placeholder={dict.placeholders.aptSuite} />
                    </div>
                    <div className='flex col-2-1'>
                        <div className='flex col-1'>
                            <Field name='billingZipCode'
                                component={Input}
                                placeholder={dict.placeholders.zipCode}
                                validate={[ required ]} />
                            <Field name='billingCity'
                                component={Select}
                                items={dict.cities}
                                placeholder={dict.placeholders.city}
                                validate={[ required ]} />
                        </div>
                        <Field name='billingState'
                            component={Select}
                            items={dict.cities}
                            placeholder={dict.placeholders.state}
                            validate={[ required ]} />
                    </div>
                    <div className='flex col-1'>
                        <Field name='billingCountry'
                            component={Input}
                            placeholder={dict.placeholders.country}
                            validate={[ required ]} />
                    </div>
                </div>
            }
        </div>
    );
}
