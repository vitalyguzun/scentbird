import React from 'react';
import { Field } from 'redux-form';

import Input from '../../../common/form/input/Input';

export default ({ subscribtionForm: dict }) => (
    <div className='card create-account'>
        <h2 className='create-account-title'>{ dict.createAccount }</h2>
        <div className='create-account-content flex col-1'>
            <Field name='email'
                component={Input}
                placeholder={dict.placeholders.email} />
            <Field name='password'
                component={Input}
                type='password'
                placeholder={dict.placeholders.password} />
        </div>
    </div>
);
