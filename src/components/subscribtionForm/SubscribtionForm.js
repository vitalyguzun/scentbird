import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { find, propEq } from 'ramda';

import validateFn from '../../utils/validators';
import { getDictionary } from '../../actions/dictionaryActions';
import Title from './cards/title/Title';
import Gender from './cards/gender/Gender';
import CreateAccount from './cards/createAccount/CreateAccount';
import ShippingAddress from './cards/shippingAddress/ShippingAddress';
import CreditCard from './cards/creditCard/CreditCard';
import Submit from './submit/Submit';
import Delivery from '../common/delivery/Delivery';

const validate = validateFn({
    email:           ['required', 'email'],
    password:        ['required', 'min10'],
    gender:          ['requiredGender'],
    firstName:       ['required'],
    lastName:        ['required'],
    shippingStreet:  ['required'],
    shippingZipCode: ['required'],
    shippingCity:    ['required'],
    shippingState:   ['required'],
    shippingCountry: ['required'],
    creditCard:      ['required', 'creditCard'],
    secureCode:      ['required', 'min3', 'notify111'],
    month:           ['required'],
    year:            ['required']
});

const form = 'subscribtionForm';

class SubscribtionForm extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired
    }

    componentWillReceiveProps({ subscribtionForm }) {
        if (subscribtionForm) {
            this.success = true;
        }
    }

    componentWillMount() {
        this.props.getDictionary(['subscribtionForm']);
    }

    componentDidMount() {
        this.props.initialize({ billingAddress: true });
    }

    onSubmit = () => {
        console.log(this.props.form);
    }

    render () {
        const product = find(propEq('id', this.props.id))(this.props.products);
        const { handleSubmit } = this.props;

        return (
            <div className='subscribtion-form-container'>
                { this.success &&
                    <form className='subscribtion-form' onSubmit={handleSubmit(this.onSubmit)} >
                        <Title {...product} className='desktop' />
                        <Gender {...this.props} {...form} />
                        <CreateAccount {...this.props} />
                        <ShippingAddress {...this.props} />
                        <CreditCard {...this.props} />
                        <Submit {...this.props} />
                        <Delivery className='mobile' />
                    </form>
                }
            </div>
        );
    }
}

const stateToProps = ({ dict: { subscribtionForm }, form, products }) => (
    { subscribtionForm, form, products }
);

SubscribtionForm = connect(stateToProps, { getDictionary })(SubscribtionForm);
export default reduxForm({ form, validate })(SubscribtionForm);
