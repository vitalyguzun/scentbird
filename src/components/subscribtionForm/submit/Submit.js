import React from 'react';
import ReactSVG from 'react-svg';
import { Button } from 'antd';

import arrowRight from './assets/arrow-right.svg';

export default ({ subscribtionForm: dict }) => (
    <div className='card submit-container'>
        <a>{ dict.buttons.back }</a>
        <Button htmlType='submit'>
            <span>{ dict.buttons.buyNow.toUpperCase() }</span>
            <ReactSVG path={ arrowRight } className='arrow-right' />
        </Button>
    </div>
);
