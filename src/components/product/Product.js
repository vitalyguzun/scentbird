import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { pick, find, propEq, path } from 'ramda';

import Title from '../subscribtionForm/cards/title/Title';
import InfoGroup from '../common/infoGroup/InfoGroup';
import Link from '../common/link/Link';
import { getProduct } from '../../actions/productsActions';
import imgs from './assets';

class Product extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired
    }

    componentWillReceiveProps({ products }) {
        if (products.length) {
            this.success = true;
        }
    }

    componentWillMount() {
        this.props.getProduct(this.props.id);
    }

    render () {
        const product = find(propEq('id', this.props.id))(this.props.products);
        const labels = path(['labels'], product);
        const values = path(['values'], product);

        return (
            <div className='product'>
                { this.success &&
                    <div>
                        <Title {...product} className='mobile' />
                        <div className='image-container'>
                            <img className='product-image' src={imgs[product.id]} alt={product.title} />
                        </div>
                        <div className='details'>
                            <InfoGroup label={labels.monthlySubscribtion} content={values.monthlySubscribtion} currency />
                            <InfoGroup label={labels.shipping} content={values.shipping} />
                            <InfoGroup label={labels.tax} content={values.tax} currency />
                            <InfoGroup label={labels.discount} content={values.discount} currency />
                            <InfoGroup label={labels.credit} content={values.credit} currency />
                        </div>
                        <div className='total'>
                            <InfoGroup label={labels.total.toUpperCase()} content={values.total} currency />
                        </div>
                        <div className='coupon'>
                            <Link {...labels.links.coupon} postfix={'?'} />
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default connect(pick(['products']), { getProduct })(Product);
