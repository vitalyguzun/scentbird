import * as Firebase from 'firebase';

const config = {
    apiKey: "AIzaSyC-z5cL_TOq0FvHPOGrCI2bQKxvbF6wAA8",
    authDomain: "scentbird-66e46.firebaseapp.com",
    databaseURL: "https://scentbird-66e46.firebaseio.com",
    projectId: "scentbird-66e46",
    storageBucket: "",
    messagingSenderId: "539658955227"
};

const fireDB = Firebase.initializeApp(config).database().ref();
export default fireDB;
