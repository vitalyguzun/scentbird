import { GET_PRODUCT } from '../constants/actionTypes';
import fireDB from './firebase-db';

export function getProduct(id) {
    return dispatch => {
        fireDB.child(`dictionary/products/${id}`).on('value', snapshot => {
            dispatch({
                type: GET_PRODUCT,
                payload: snapshot.val()
            });
        });
    };
}
