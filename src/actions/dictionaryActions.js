import { GET_DICTIONARY } from '../constants/actionTypes';
import fireDB from './firebase-db';

export function getDictionary(path = []) {
    return dispatch => {
        fireDB.child(`dictionary/${path.join('/')}`).on('value', snapshot => {
            dispatch({
                type: GET_DICTIONARY,
                payload: { [path[0]]: snapshot.val() }
            });
        });
    };
}
