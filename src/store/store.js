import { applyMiddleware, combineReducers, createStore } from 'redux';
import reduxThunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import rootReducer from '../reducers/rootReducer';

let middlewares = [reduxThunk, createLogger({collapsed: true})];

const Store = createStore(
    combineReducers({ ...rootReducer }),
    undefined,
    applyMiddleware(...middlewares)
);

export default Store;
